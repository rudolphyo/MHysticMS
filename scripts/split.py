import pandas as pd
import numpy as np
import argparse

from sklearn.model_selection import train_test_split


np.random.seed(42)

parser = argparse.ArgumentParser()

parser.add_argument("--data_path", "--dp", type=str, default="./data/ms_peptides.csv.gz", help="Path to MS data to split on train and test")
parser.add_argument("--test_size", "--ts", type=float, default=0.4, help="Fraction of data to hold for test")

args = parser.parse_args()

flanks = pd.read_csv(args.data_path)

flanks.nseq = flanks.nseq.apply(lambda x: "" if type(x) != str else x)
flanks.cseq = flanks.cseq.apply(lambda x: "" if type(x) != str else x)

flanks.nseq = flanks.nseq.apply(lambda x: x + (5 - len(x)) * "X")
flanks.cseq = flanks.cseq.apply(lambda x: (5 - len(x)) * "X" + x)

train_inds = []
test_inds = []

for cur_mhc in flanks.mhc.unique():
    mhc_flanks = flanks.loc[flanks.mhc == cur_mhc]
    zeros = np.where(mhc_flanks.hit.values == "-")[0]
    ones = np.where(mhc_flanks.hit.values == "+")[0]

    train_zeros, test_zeros = train_test_split(zeros, test_size=args.test_size)
    train_ones, test_ones = train_test_split(ones, test_size=args.test_size)

    train_inds.append(train_zeros)
    train_inds.append(train_ones)

    test_inds.append(test_zeros)
    test_inds.append(test_ones)

train_flanks = flanks.iloc[np.concatenate(train_inds)]
test_flanks = flanks.iloc[np.concatenate(test_inds)]

train_flanks.to_csv("./data/train.csv.gz", index=False, compression="gzip")
test_flanks.to_csv("./data/test.csv.gz", index=False, compression="gzip")
