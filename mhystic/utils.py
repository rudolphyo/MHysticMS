import pandas as pd
import numpy as np

from sklearn.preprocessing import MultiLabelBinarizer

def divide_preprocess(flanks_):
    """
    separate_df function takes flanks pandas DataFrame containing MS data for hits and sampled decoys
    and returns numpy array for furher processing with tf.Data
    """
    flanks_one = flanks_.loc[flanks_.hit == "+"]
    flanks_zero = flanks_.loc[flanks_.hit == "-"]

    return df2numpy(flanks_one), df2numpy(flanks_zero)

def df2numpy(flanks_):
    """
    df2numpy function takes flanks pandas DataFrame containing MS data for hits and sampled decoys
    and returns numpy array for furher processing with tf.Data
    """

    aminoacids = "ARNDCEQGHILKMFPSTWYVX"
    acids_idxs = {i:ind for ind, i in enumerate(aminoacids)}

    pseudoseqs = pd.read_csv("./data/pseudoseqs.csv.gz")
    pseudo_dict = {i:j for i, j in zip(pseudoseqs.mhc.values, pseudoseqs.pseudo.values)}

    flanks_df = flanks_.copy()
    MAX_LEN = max(flanks_df.sequence.str.len())

    flanks_df.nseq = flanks_df.nseq.apply(lambda x: "" if type(x) != str else x)
    flanks_df.cseq = flanks_df.cseq.apply(lambda x: "" if type(x) != str else x)
    flanks_df.mhc = flanks_df.mhc.apply(lambda x: "HLA" + x)
    flanks_df.hit = flanks_df.hit.map({"+":1, "-":0})
    mhc_seqs = [pseudo_dict[i] for i in flanks_df.mhc.values]
    pep_seqs = flanks_df.sequence.apply(lambda x: x + (15 - len(x)) * "X")

    mhcs = np.array([list(map(acids_idxs.get, i)) for i in mhc_seqs])
    peps = np.array([list(map(acids_idxs.get, i)) for i in pep_seqs])

    pep_lens = np.array(flanks_df.sequence.str.len()).reshape(-1, 1)
    nseq_lens = np.array(flanks_df.nseq.str.len()).reshape(-1, 1)
    cseq_lens = np.array(flanks_df.cseq.str.len()).reshape(-1, 1)

    nseqs_x = flanks_df.nseq.apply(lambda x: x + (5 - len(x)) * "X")
    cseqs_x = flanks_df.cseq.apply(lambda x: (5 - len(x)) * "X" + x)

    nseqs = np.array([list(map(acids_idxs.get, i)) for i in nseqs_x])
    cseqs = np.array([list(map(acids_idxs.get, i)) for i in cseqs_x])

    nposs = flanks_df.npos.values.reshape(-1, 1)
    cposs = flanks_df.cpos.values.reshape(-1, 1)
    msmss = flanks_df.mhc_msms.values.reshape(-1, 1)
    geneexprs = flanks_df.gene_expr.values.reshape(-1, 1)
    hls = flanks_df.half_life.values.reshape(-1, 1)

    ohe_pep_lens = MultiLabelBinarizer(classes=[8, 9, 10, 11, 12, 13, 14, 15])
    ohe_ncseq_lens = MultiLabelBinarizer(classes=[0, 1, 2, 3, 4, 5])
    ohe_labels = MultiLabelBinarizer(classes=[0, 1])

    pep_lens = ohe_pep_lens.fit_transform(pep_lens)
    nseq_lens = ohe_ncseq_lens.fit_transform(nseq_lens)
    cseq_lens = ohe_ncseq_lens.fit_transform(cseq_lens)
    labels = ohe_labels.fit_transform(flanks_df.hit.values.reshape(-1, 1))
    meta_features = np.hstack((nposs, cposs, msmss, geneexprs, hls)).astype(np.float32)

    to_drop = np.unique(np.concatenate([np.where(pd.isnull(i) == True)[0] for i in [nseqs, mhcs, peps, cseqs]]))
    mask = np.ones(labels.shape[0], dtype=bool)
    mask[to_drop] = False

    return (mhcs[mask], peps[mask].astype(int), pep_lens[mask].astype(np.float32)), labels[mask]

