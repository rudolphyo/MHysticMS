from mhystic.mhystic import *

class MHysticBlock(MHystic):

    def _init_embedding(self, seq_len):
        train = False if self._load else True
        return tf.keras.layers.Embedding(output_dim=self._params.emb_d,
                                         input_dim=21,
                                         trainable=train,
                                         input_length=seq_len,
                                         embeddings_initializer=tf.keras.initializers.he_uniform())

    def _block(self, prev_inp, in_f, out_f, cur_s):
        shortcut = tf.layers.batch_normalization(prev_inp, training=self._train_phase, fused=False)
        shortcut = tf.keras.layers.PReLU()(shortcut)
        shortcut_conv = tf.layers.conv1d(shortcut, out_f, cur_s, padding="valid",
                                         kernel_initializer=tf.keras.initializers.he_normal())

        branch = tf.layers.conv1d(shortcut, in_f, cur_s, padding="valid",
                                  kernel_initializer=tf.keras.initializers.he_normal())
        branch = tf.layers.batch_normalization(branch, training=self._train_phase, fused=False)
        branch = tf.keras.layers.PReLU()(branch)
        branch = tf.layers.dropout(branch, self._params.dp_rate, training=self._train_phase)
        branch = tf.layers.conv1d(branch, out_f, cur_s, padding="same",
                                  kernel_initializer=tf.keras.initializers.he_normal())
        return tf.add(branch, shortcut_conv)

    def _make_graph(self, mhcs, peps, pep_lens, nseqs, nseq_lens, cseqs, cseq_lens, meta_features):
        with tf.name_scope("MHCBranch"):
            mhc_branch = self._init_embedding(34)(mhcs)
            mhc_branch = tf.layers.dropout(mhc_branch, self._params.idp_rate, training=self._train_phase)
            for ind, i in enumerate(range(self._params.nb_mhc)):
                in_f, out_f = 2**(4+i), 2**(5+i)
                mhc_branch = self._block(mhc_branch, in_f, out_f, (ind+1)*3)
            mhc_branch = tf.layers.batch_normalization(mhc_branch, training=self._train_phase)
            mhc_branch = tf.keras.layers.PReLU()(mhc_branch)
            mhc_branch = tf.layers.flatten(mhc_branch)

        with tf.name_scope("PeptideBranch"):
            pep_branch = self._init_embedding(11)(peps)
            for ind, i in enumerate(range(self._params.nb_pep)):
                in_f, out_f = 2**(2+i), 2**(3+i)
                pep_branch = self._block(pep_branch, in_f, out_f, ind+1)
            pep_branch = tf.layers.batch_normalization(pep_branch, training=self._train_phase)
            pep_branch = tf.keras.layers.PReLU()(pep_branch)
            pep_branch = tf.layers.flatten(pep_branch)

        with tf.name_scope("N-SeqBranch"):
            nseq_branch = self._init_embedding(5)(nseqs)
            nseq_branch = self._block(nseq_branch, 4, 8, 1)
            nseq_branch = tf.layers.batch_normalization(nseq_branch, training=self._train_phase)
            nseq_branch = tf.keras.layers.PReLU()(nseq_branch)
            nseq_branch = tf.layers.flatten(nseq_branch)
            nseq_branch = tf.concat([nseq_branch, nseq_lens], 1)
            nseq_branch = tf.layers.dense(nseq_branch, 32, kernel_initializer=tf.keras.initializers.he_normal())
            nseq_branch = tf.layers.dropout(nseq_branch, self._params.dp_rate, training=self._train_phase)

        with tf.name_scope("C-SeqBranch"):
            cseq_branch = self._init_embedding(5)(cseqs)
            cseq_branch = self._block(cseq_branch, 4, 8, 1)
            cseq_branch = tf.layers.batch_normalization(cseq_branch, training=self._train_phase)
            cseq_branch = tf.keras.layers.PReLU()(cseq_branch)
            cseq_branch = tf.layers.flatten(cseq_branch)
            cseq_branch = tf.concat([cseq_branch, cseq_lens], 1)
            cseq_branch = tf.layers.dense(cseq_branch, 32, kernel_initializer=tf.keras.initializers.he_normal())
            cseq_branch = tf.layers.dropout(cseq_branch, self._params.dp_rate, training=self._train_phase)

        with tf.name_scope("MetaMS"):
            meta_branch = tf.layers.dense(meta_features, 32, kernel_initializer=tf.keras.initializers.he_normal())
            meta_branch = tf.layers.dropout(meta_branch, self._params.dp_rate, training=self._train_phase)

        with tf.name_scope("Merged"):
            merged_pep_mhc = tf.concat([pep_branch, mhc_branch], 1)
            merged_pep_mhc = tf.layers.dense(merged_pep_mhc, 128,
                                             kernel_initializer=tf.keras.initializers.he_normal())
            merged_pep_mhc = tf.layers.batch_normalization(merged_pep_mhc, training=self._train_phase)
            merged_pep_mhc = tf.keras.layers.PReLU()(merged_pep_mhc)
            merged_pep_mhc = tf.layers.dropout(merged_pep_mhc, self._params.dp_rate, training=self._train_phase)
            merged_pep_mhc = tf.concat([merged_pep_mhc, pep_lens], 1)
            merged_pep_mhc = tf.layers.dense(merged_pep_mhc, 64,
                                             kernel_initializer=tf.keras.initializers.he_normal())
            merged_pep_mhc = tf.layers.batch_normalization(merged_pep_mhc, training=self._train_phase)
            merged_pep_mhc = tf.keras.layers.PReLU()(merged_pep_mhc)
            merged_pep_mhc = tf.layers.dropout(merged_pep_mhc, self._params.dp_rate, training=self._train_phase)

            merged_all = tf.concat([merged_pep_mhc, meta_branch, nseq_branch, cseq_branch], 1)
            merged_all = tf.layers.dense(merged_all, 128, kernel_initializer=tf.keras.initializers.he_normal())
            merged_all = tf.layers.batch_normalization(merged_all, training=self._train_phase)
            merged_all = tf.keras.layers.PReLU()(merged_all)
            merged_all = tf.layers.dropout(merged_all, self._params.dp_rate, training=self._train_phase)
            merged_all = tf.layers.dense(merged_all, 64, kernel_initializer=tf.keras.initializers.he_normal())
            merged_all = tf.layers.batch_normalization(merged_all, training=self._train_phase)
            merged_all = tf.keras.layers.PReLU()(merged_all)
            merged_all = tf.layers.dropout(merged_all, self._params.dp_rate, training=self._train_phase)

            merged_all = tf.layers.dense(merged_all, 2, name="Logits")
        return merged_all

