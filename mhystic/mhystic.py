import tensorflow as tf
import gc
import numpy as np
import time
import os
from sklearn.metrics import f1_score

from mhystic.utils import divide_preprocess, df2numpy

class MHystic:

    def __init__(self, params, checkpoint, load_weights=False):
        """
        Initialize Mhystic model with given parameters of architecture
        Parameters:

        """
        self._graph = tf.Graph()
        self._params = params
        self.checkpoint = checkpoint
        self._load = load_weights
        tf.logging.set_verbosity(tf.logging.INFO)

    def _init_iterator(self, data, is_train):
        """
        """
        if is_train:
            np_hits, np_not_hits = divide_preprocess(data)
            hits = tf.data.Dataset.from_tensor_slices(np_hits).repeat(np_not_hits[1].shape[0] // np_hits[1].shape[0])
            not_hits = tf.data.Dataset.from_tensor_slices(np_not_hits)
            dataset = tf.data.Dataset.zip((hits, not_hits)).shuffle(buffer_size=2000)
            dataset = dataset.flat_map(
                lambda p, n:tf.data.Dataset.from_tensors(p).concatenate(tf.data.Dataset.from_tensors(n))
            )
        else:
            data = df2numpy(data)
            dataset = tf.data.Dataset.from_tensor_slices(data)
        dataset = dataset.batch(self._params.batch_size).prefetch(self._params.batch_size)
        return dataset

    def _init_graph(self, train, test):
        """
        """
        self._train_phase = tf.placeholder(tf.bool, [], name='TrainingFlag')
        self._global_step = tf.Variable(0, trainable=False)

        with tf.variable_scope("Datasets"):
            self._train_dataset = self._init_iterator(train, is_train=True)
            self._test_dataset = self._init_iterator(test, is_train=False)
            self._iterator= tf.data.Iterator.from_structure(self._train_dataset.output_types, self._train_dataset.output_shapes)
            self._train_init = self._iterator.make_initializer(self._train_dataset)
            self._test_init = self._iterator.make_initializer(self._test_dataset)
            features, labels = self._iterator.get_next()
        with tf.device('gpu'):
            with tf.name_scope("Prediction"):
                logits = self._make_graph(*features)
                self._predictions = tf.nn.softmax(logits, name="SoftmaxResult")

            with tf.name_scope("Loss"):
                loss = tf.losses.softmax_cross_entropy(labels, logits)

            with tf.variable_scope("Optimizer"):
                learning_rate = tf.train.exponential_decay(1e-3, self._global_step,
                                                           1000, 0.97, staircase=True)
                train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss, global_step=self._global_step)

        with tf.name_scope("Metrics"):
            self._accuracy, self._accuracy_op = tf.metrics.accuracy(tf.argmax(self._predictions, axis=1),  tf.argmax(labels, axis=1))
            self._mean_loss, self._mean_loss_op = tf.metrics.mean(loss)
            self._precision, self._precision_op = tf.metrics.precision(tf.argmax(self._predictions, axis=1),  tf.argmax(labels, axis=1))
            self._recall, self._recall_op = tf.metrics.recall(tf.argmax(self._predictions, axis=1),  tf.argmax(labels, axis=1))
            self._f1 = 2 * self._precision * self._recall / (self._precision + self._recall)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        update_ops += [self._accuracy_op, self._mean_loss_op]
        self._train_op = tf.group(update_ops + [train_op])

        self._train_writer = tf.summary.FileWriter(os.path.join(self.checkpoint, "train"))
        self._train_writer.add_graph(tf.get_default_graph())
        self._test_writer = tf.summary.FileWriter(os.path.join(self.checkpoint,"test"))
        lr_summary = tf.summary.scalar("LearningRate", learning_rate)
        loss_summary = tf.summary.scalar("CrossEntropy", loss)
        accuracy_summary = tf.summary.scalar("Accuracy", self._accuracy)
        ce_summary = tf.summary.scalar("CrossEntropy", self._mean_loss)
        pr_summary = tf.summary.scalar('Precision', self._precision)
        r_summary = tf.summary.scalar('Recall', self._recall)
        f1_summary = tf.summary.scalar('F1_Measure', self._f1)
        self._train_summary = tf.summary.merge([lr_summary, loss_summary])
        self._test_summary = tf.summary.merge([accuracy_summary, ce_summary, pr_summary, r_summary, f1_summary])

        self._saver = tf.train.Saver()
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.45)
        self._session = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
        #self._session = tf.Session()

        n_params = np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])
        tf.logging.warning("Number of trainable parameters: {}".format(n_params))

        self._session.run(tf.global_variables_initializer())
        self._session.run(tf.local_variables_initializer())

    def train(self, train, test):
        tf.logging.info("Initializing graph...")
        self._init_graph(train, test)
        tf.logging.info("Start training...")
        for cur_epoch in range(1, self._params.epochs + 1):
            epoch_start = time.time()
            tf.logging.info("Epoch {}/{}...".format(cur_epoch, self._params.epochs))
            self._session.run(self._train_init)
            self._session.run(tf.local_variables_initializer())
            while True:
                try:
                    _, summary = self._session.run([self._train_op, self._train_summary], feed_dict={self._train_phase:True})
                    acc, loss = self._session.run([self._accuracy, self._mean_loss])
                    self._train_writer.add_summary(summary, self._global_step.eval(session=self._session))
                except (tf.errors.OutOfRangeError, StopIteration):
                    tf.logging.info("Cross-Entropy loss: {:.4f}, Accuracy: {:.4f}".format(loss, acc))
                    break
            self._session.run(self._test_init)
            self._session.run(tf.local_variables_initializer())
            while True:
                try:
                      *_, summary = self._session.run([self._accuracy_op, self._mean_loss_op, self._precision_op,
                                                       self._recall_op, self._f1, self._test_summary], feed_dict={self._train_phase:False})
                except (tf.errors.OutOfRangeError, StopIteration):
                    tf.logging.warning("Test evaluation, F1-Score: {:.4f}, epoch took {:.4f}s".format(self._f1.eval(session=self._session), time.time() - epoch_start))
                    self._test_writer.add_summary(summary, cur_epoch)
                    break

