import os
import argparse
import numpy as np
import pandas as pd

from collections import namedtuple
from importlib import import_module as import_arch
from mhystic.utils import divide_preprocess

parser = argparse.ArgumentParser()
parser.add_argument("--embedding_dim", "--dim", type=int, default=40,
                    help="Dimension of embeddings for aminoacids")
parser.add_argument("--mhc_blocks", "--mb",  type=int, default=2,
                    help="Number of blocks for mhc branch")
parser.add_argument("--pep_blocks", "--pb",  type=int, default=2,
                    help="Number of blocks for peptide branch")
parser.add_argument("--nseq_blocks", "--nb", type=int, default=1,
                    help="Number of residual blocks for n-seq branch")
parser.add_argument("--cseq_blocks", "--cb", type=int, default=1,
                    help="Number of residual blocks for c-seq branch")
parser.add_argument("--batch_size", "--bs", type=int, default=512,
                    help="Batch size during training")
parser.add_argument("--out_folder", "--of", type=str,
                    help="Path for summarry and weights to store")
parser.add_argument("--epochs", "--e", type=int, default=50,
                    help="Number of training epochs")
parser.add_argument("--arch_path", "--arch", type=str,
                    help="Path to .py file with network architecture")
parser.add_argument("--prefix_name", "--pn", type=str,
                    help="Prefix of model name")

args = parser.parse_args()
mhystic = import_arch(args.arch_path)

MhysticConfig = namedtuple("MHysticConfig", ["emb_d", "nb_mhc", "nb_pep", "nb_nseq", "nb_cseq", "mhc_d", "pep_d", "ncseq_d",
                                             "dp_rate", "idp_rate", "batch_size", "epochs", "pep_lens_dim", "meta_dim"])

print("Loading and processing train data...")
train = pd.read_csv("./data/train.csv.gz")
print("Shape of train samples (all features): {}".format(train.shape))

print("Loading and processing test data...")
test = pd.read_csv("./data/test.csv.gz")
print("Shape of test samples (all features): {}".format(test.shape))

config = MhysticConfig(emb_d=args.embedding_dim, nb_mhc=args.mhc_blocks, nb_pep=args.pep_blocks,
                       nb_nseq=args.nseq_blocks, nb_cseq=args.cseq_blocks, mhc_d=34, pep_d=13, ncseq_d=5,
                       dp_rate=0.3, idp_rate=0.1, batch_size=args.batch_size, epochs=args.epochs,
                       pep_lens_dim=len(range(8, 13)), meta_dim=5)

checkpoint = os.path.join(args.out_folder, args.arch_path[11:] + args.prefix_name)
MhysticNN = mhystic.MHysticBlock(config, checkpoint, load_weights=False)
print("Starting training {} model".format(args.prefix_name))
MhysticNN.train(train, test)
